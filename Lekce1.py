from turtle import*
# Naše "načtení knihovny želva", abychom mohli kreslit
# Kód co něco dělá je níže ->

# Ty mřížky (#) vytvářejí tzv. komentáře, část kódu, která nedělá nic, 
# ale můžete si do ní zapsat co potřebujete navíc mimo program (třeba co program dělá). 

# Udělej něco 4x 
for i in range(4):
    # Zatoč o 90 stupňů
    rt(90)
    # Jeď rovně o 100 pixelů
    fd(100)