from turtle import*
import turtle
pensize(1)
speed(0)

pensize(10)
for d in range(25):
    for i in range(25):
      r = 0.2 + i/30.0 * (d/25)
      g = 0.2 + i/30.0 * ((25 - d)/25)
      b = 0.2 + i/30.0
      color(r,g,b)
      fd(10)
        
    penup()
    fd(-250)
    lt(90)
    fd(10)
    rt(90)
    pendown()

  