from turtle import*

rt(80) # 80 stupňů v tomto případě nechceme pravý úhel
pensize(3)   # tlouštka pera
speed(8)     # nastavení rychlosti
for i in range(10):  
    fd(50)
    lt(160)    # zatáčíme doleva
    fd(50)
    rt(160)    # a zpátky doprava

