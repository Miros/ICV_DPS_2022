"""
Vítejte v replu! Tímto způsobem budou v tomto kurzu prezentované úryvku kódu, které si zde můžete prohlédnout, spustit a rovnou začít editovat.

Poznámka: tři uvozovky značí víceřádkový komentář, který nemá na běh programu žádný efekt.
"""

x = 5
text = "hi"

print ("Proměnná x má nastavenou hodnotu na: ", x)
print ("Proměnná text má nastavenou hodnotu na: ", text)

print ("S proměnou můžeme dělat spoustu operací: ", (x * 8)/3 + 5)

# Uvnitř kódu můžeme psát tyto jednořádkové komentáře,
# které jej nijak neovlivňují
# Přepsat hodnotu proměnné můžeme jednoduše:
x = x + 5
print ("Nyní je hodnota x: ", x)

# Podmínka umožňuje otestovat zdali je nějaký výrok pravdivý,
# zde například testujeme jestli je hodnota x rovna 10
if x == 10:
  print ("Nyní je hodnota x 10!" )
else:  # jinak ...
  print ("Hodnota x není 10")  
