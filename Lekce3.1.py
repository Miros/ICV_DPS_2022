from turtle import*
pensize(3)   # tloušťka pera
speed(8)     # nastavení rychlosti

for pocet in range(8):  # počet čtverců
    # Výběr barvy
    if pocet%2 == 0:  # sudé
        color("red")
    else: # "ne-sude" liché
        color("blue")
                    
    for ctverec in range(4): # nakresli čtverec
        fd(50)
        rt(90)  
    fd(50) # posuň se na další čtverec