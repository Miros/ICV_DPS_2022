# ICV DPS 2022
 Git projekt zaměřený na publikaci kurzu základu programování pro ICV-DPS.

## Úvodem
V tomto kurzu budeme používat repozitářový a vývojový systém [repl.it](https://replit.com/) a s jeho pomocí se naučíme základy programování v [pythonu](https://www.python.org/) s knihovnou [želva (turtle)](https://docs.python.org/3/library/turtle.html).
Pro tento kurz je vhodná alespoň základní znalost programování. Abychom se však snáze dostali na výchozí úroveň můžete použít kurz pythonu na internetu například od w3schools, kde úvod pokrývají [lekce 1](https://www.w3schools.com/python/python_intro.asp) a [lekce 3](https://www.w3schools.com/python/python_syntax.asp).

Jednotlivé kódy jsou vloženy pomocí odkazů, které se vždy jmenují: Kód k lekci N. (Kde N je číslo lekce.)
Dále jsou vhodné alespoň základní znalosti z matematiky, jsou používaný pojmy jako "výraz" a aplikované nějaké ze základních znalostí geometrie.
Samotný kurz začíná úvodem do cyklů v programování.

## Lekce 0
Pro jistotu zde přikládám ukázkový okomentovaný kód, který ukáže úvod, na kterém bychom měli zakládat. Prohlédnutí a spuštění kódů nevyžaduje nic než navšítívit přiložený odkaz, pro editování kódů je třeba mít účet na  Repl.it a s ním vlastní úložný prostor na svoje programy. Registraci silně doporučuji, stránka se bude i načítat rychleji a bude přehlednější, protože kód a jeho výsledek budou vidět zároveň na jedné obrazovce.

Program spustíme zeleným tlačítkem "run" vpravo nahoře v okně kódu. Pro editaci stačí stisknout modré tlačítko "Fork repl" vpravo. Většina pozdějších programů počítá právě s tímto postupem, kdy je velmi vhodné, abyste si kód sami otevřeli a editovali.
[Kód k lekci 0.](https://replit.com/@MirekNovotn/Lekce0#main.py)

> Poznámka: Repl.it podporuje diakritiku, tak to však nemusí být ve
> všech programovacích prostředních (často není), takže na závěr
> doporučuji (obzvláště u proměnných) používat anglické názvy. Zde je
> čeština použita v komentářích a vysvětlivkách pro zjednodušení kurzu.

![image](https://gitlab.com/Miros/ICV_DPS_2022/-/raw/main/Lekce0.0.png)

## Lekce 1
Nyní budete vyrábět program, ve kterém použijete slíbený cyklus. Zatím jej nebudete muset psát vy, jen budete upravovat kód ode mne.

V kódu je použitý příkaz cyklu
`for i in range(4):` 
Tento kód umožňuje opakování a provede níže uvedený **odsazený** kód tolikrát kolikrát je napsáno v `range`.
Odsazení je důležité neboť nám určuje takzvané bloky kódu.
Kód:
```
for i in range(4): 
   print("ahoj")
   print("nazdar")
```
Vypíše 4 krát ahoj a 4 krát nazdar.
Zatímco kód:
```
for i in range(4): 
   print("ahoj")
print("nazdar")
```
Vypíše 4 krát ahoj a 1 krát nazdar na závěr.

> Poznámka: Stejným způsobem je možné "zanořit" jeden cyklus do druhého, stačí, aby následující cyklus byl v bloku toho předchozího.

Slovo `for` pouze říká pythonu, že provádíme cyklus. proměnná `i`, která  následuje postupně nabývá hodnoty podle jednotlivých kroků cyklu, při první průběhu bude mít hodnotu 0, poté 1 a tak dále, pro `range(4)` je finální hodnota 3, protože číslujeme od nuly. Uvnitř cyklu můžeme z touto proměnou pracovat jako s každou jinou proměnou, můžeme ji tedy vložit do podmínky nebo výrazu. Tato proměnná se nemusí jmenovat `i`, může se jmenovat jakkoliv.

> Poznámka: V zanořených cyklech je třeba pojmenovat proměnné cyklů různě.

Dále používáme příkazy (funkce) `fd` pro nakreslení rovné čáry a `rt` pro zatočení doprava. Těmto funkcím se musí předat hodnota o kolik mají "jet dopředu" nebo "zatočit" a proto je potřeba je používat jako `fd(100)` nebo `rt(90)`, kde se posouváme o  100 bodů dopředu a zatáčíme o 90 stupňů.

Kód: 
```
for ahoj in range(4): 
   print(ahoj)
```
Tedy vypíše :
```
1
2
3
4
```
Zde je ukázkový kód k lekci a k zadaným úkolům:
[Kód k lekci 1.](https://replit.com/@MirekNovotn/Lekce1#main.py)

Úkol 1. Upravte kód, aby místo čtverce nakreslil trojúhelník (bez odebírání nebo přidávání řádků, úpravou čísel).  
Úkol 2, Nakreslete čtverec, s poloviční délkou strany, než je v příkladu. (Je plocha bude 4x menší.)
Komentáře v kódu můžete vymazat.
Nějaké další čtení k cyklu for v pythonu můžete nalézt například na [této stránce w3schools](https://www.w3schools.com/python/python_for_loops.asp). 

## Lekce 2
V této lekci pokračujte ve vašem vlastní experimentování s programováním, vyzkoušejte si například vykreslení pily:
[Kód k lekci 2.](https://replit.com/@MirekNovotn/Lekce2#main.py)
Přibývá nám několik nových příkazů `lt` je ekvivalentem `rt` a umožňuje nám zatočit doleva. 
Další příkazy jsou spíše pro ukázku a usnadnění použití.
`speed(8)` nastavuje rychlost animace kreslení, kde 1-10 je 10 nejrychlejší a pak 0 úplně bez animace (nejrychlejší).
`pensize(3)` nám umožňuje vybrat jinou tloušťku kreslícího pera, výchozí nastavení je 1.

> Poznámka: V pozdějších lekcích přibude ještě funkce "color" pro nastavení barvy, ale tato knihovna podporuje také funkci "shape", kde shape("turtle") nastaví, že místo šipky bude po obrazovce jezdit želva, tento příkaz nemá žádný dopad na program ani průběh kódu je čistě vizuální.

Uvnitř fukncí se stejně jako čísla mohou vyskytovat proměnné nebo výrazy pokud tedy budeme chtít zatočit o úhel odpovídající nějakému n-úhelníku můžeme provést tento výpočet:
```
n = 3
rt(360/n)
```
A program zatočí o 120 stupňů což odpovídá trojúhelníku (3). Pokud by proměnná n byla nastavena na hodnotu například 12, byl by výsledný úhel 30 stupňů.

Úkol 1. Upravte kód tak, aby byly zuby pily kratší a pila delší.
Úkol 2. Vytvořte vlastní nový kód, který nakreslí šestiúhelník.

> Poznámka: Vlastní nový repl vytvoříte kliknutím na menu tlačítko vlevo nahoře, kde kliknete na modré tlačítko + Create.

## Lekce 3
V této lekci si ukážeme další nové příkazy a dva nové programy pro vás k editování. Programy v této lekci již budou o něco složitější 
Na úvod ale nedříve nový příkaz, "color", který umožňuje změnit barvu, kteoru se právě kreslí na obrazovku. Barva je zvolena v angličtině, přičemž program podporuje několik desítek zákadních barev.
Například `color("green")` změní barvu všech následujících čar na zelenou.
Na zkoušku si zkuste nejdříve domyslet co níže uvedený program udělá a poté si jej ve vlastnoručně vytvořeném replu otestuje. 
    for i in range(2):
    	    color("blue")
    	    fd(100)
    	    rt(90)
    	    color("red")
    		fd(100)
    	    rt(90)

[Kód k lekci 3 ukázka 1.](https://replit.com/@MirekNovotn/Lekce31#main.py)

[Kód k lekci 3 ukázka 2.](https://replit.com/@MirekNovotn/Lekce32#main.py )

> Poznámka v této lekci jsou úmyslně zvoleny příklady trochu obtížnějších programů, na úkázku co se dá pomocí již probraných funkcí a nástrojů například dosáhnout.

V této lekci si za úkol ukázkové příklady vyzkoušejte a zamyslete se jakým způsobem je dosaženo jejich výsledků tedy, co způsobuje stále se zvětšující kresbu u spirály a díky čemu se jednotlivé čtverce kreslí vedle sebe.

## Lekce 4

Tuto lekci provedeme obrácený postup oproti lekcím předchozím, zkusíte si napsat vlastní kód a na závěr pak tento kód "zkontrolovat" oproti mnou dodanému referenčnímu řešení. 

Napište cyklus, který nakreslí jednu polovinu "kružnice"

![image](https://gitlab.com/Miros/ICV_DPS_2022/-/raw/main/Lekce4.1.png)

Tipy jak na to:
1.  Nepřesná kružnice je vlastně takový 360-ti úhelník.
2.  Příkaz speed(0) udělá, že to poběží co nejrychleji (musí být napsaný na začátku).
3.  Příkaz pensize(2) způsobí, že se bude kreslit tlustší čára.
4.  Jinak budete potřebovat hlavně fd a rt nebo lt.

> Poznámka: Jiný postup neznamená, že je postup špatný! Programování nabízí spoustu nástrojů a typicky existuje spousta cest jak dosáhnout požadovaného výsledku, nebojte se experimentovat.

[Kód k lekci 4 řešení 1.](https://replit.com/@MirekNovotn/Lekce41#main.py)

Další na řadě je konečně promluvit o tom jak můžeme využít proměnou `i` (která se tak vůbec nemusí jmenovat). Nakreslete takovouto čáru: (Nepravidelnost barev nemusí být přesně dle obrázku.) 

![image](https://gitlab.com/Miros/ICV_DPS_2022/-/raw/main/Lekce4.2.png) 

V tomto případě budeme potřebovat pracovat s proměnou cyklu a to tak, že ji správně vložíme do výrazu v podmínce. Můžeme použít i operátor `or` (nebo), který spojuje několik výrazů za sebe podmínka je považována za splněnou pokud je alespoň jeden z nich pravdivý. (Pozn. funguje i `and` jako "a", které provede podmínku pouze tehdy pokud jsou všechny výrazy splněné, například `if i > 5 and i < 9:` bude splněné pro hodnoty 6,7 a 8.)

[Kód k lekci 4 řešení 2.](https://replit.com/@MirekNovotn/Lekce-42#main.py)

## Lekce 5

Několikrát jsme již používali v želvě příkaz "color", kde jsme pomocí názvu zvolili barvu. Funkce "color" však vezme i čísla a to například RGB.
Tedy například příkaz `color(1,0,0)` nastaví barvu na červenou. Pro výběr barvy můžete použít například [tento](https://www.w3schools.com/colors/colors_picker.asp) výběrník barvy. čísla, která vám dává v řádku: rgb(51, 102, 255), však budete muset vydělit 255 a což se vám python bez problémů postará.

[Kód k lekci 5.](https://replit.com/@MirekNovotn/Lekce5#main.py)

Zkuste za úkol nakreslit barevný čtverec:  
Například:  

![image](https://gitlab.com/Miros/ICV_DPS_2022/-/raw/main/Lekce5.png)

(Barvy a přechody si vymyslete jak budete chtít, pomocné čáry nevadí.)
[Kód k lekci 5. Závěrečné řešení](https://replit.com/@MirekNovotn/Lekce51#main.py)
